import React from 'react';
import Link from 'next/link';
//import styles from "../styles/Home.module.css";
import { Card, Col, Image } from 'react-bootstrap';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Col className="mb-4">
          <Card className="py-5 shadow-lg h-100">
            <Card.Body>
              <Card.Title>{this.props.library.title}</Card.Title>
              <p>{this.props.library.description}</p>
            </Card.Body>
            <Link href={this.props.library.url} passHref>
              <Image
                src={this.props.library.img}
                alt={this.props.library.title}
                //className={styles.img}
                style={{ cursor: 'pointer' }}
              />
            </Link>
          </Card>
        </Col>
      </React.Fragment>
    );
  }
}

export default ListItem;
