import { render, screen } from '@testing-library/react';
import Admin from '@/pages/admin';

describe('Test admin route', () => {
  it('does it render content', () => {
    render(<Admin />);
    const title = screen.getByText('Admin Panel');
    expect(title).toBeInTheDocument();
  });

  it('does the snapshot match', () => {
    const { container } = render(<Admin />);
    expect(container).toMatchSnapshot();
  });
});
