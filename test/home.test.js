import { render, screen } from '@testing-library/react';
import Home from '@/pages/index';

describe('Test home route', () => {
  it('does it render content', () => {
    render(<Home />);
    const heading = screen.getByRole('heading', {
      name: /Automated software testing/i,
    });
    expect(heading).toBeInTheDocument();
  });

  it('does the snapshot match', () => {
    const { container } = render(<Home />);
    expect(container).toMatchSnapshot();
  });
});
