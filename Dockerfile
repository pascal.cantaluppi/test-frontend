# build environment
FROM node:17-alpine as builder
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build

# production environment
FROM node:17-alpine
WORKDIR /app
COPY --from=builder /app/package.json .
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/public ./public
COPY --from=builder /app/node_modules ./node_modules
CMD ["npm", "start"]
